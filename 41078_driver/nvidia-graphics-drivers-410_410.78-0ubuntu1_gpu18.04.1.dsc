-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: nvidia-graphics-drivers-410
Binary: nvidia-driver-410, nvidia-kernel-source-410, nvidia-dkms-410, nvidia-utils-410, libnvidia-compute-410, nvidia-compute-utils-410, nvidia-headless-no-dkms-410, nvidia-headless-410, nvidia-kernel-common-410, libnvidia-gl-410, libnvidia-common-410, xserver-xorg-video-nvidia-410, libnvidia-cfg1-410, libnvidia-ifr1-410, libnvidia-fbc1-410, libnvidia-decode-410, libnvidia-encode-410
Architecture: amd64 i386 all
Version: 410.78-0ubuntu1~gpu18.04.1
Maintainer: Ubuntu Core Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders:  Alberto Milone <alberto.milone@canonical.com>,
Homepage: http://www.nvidia.com
Standards-Version: 4.1.1
Vcs-Browser: https://github.com/tseliot/nvidia-graphics-drivers/tree/410
Vcs-Git: git://github.com/tseliot/nvidia-graphics-drivers.git
Build-Depends: debhelper (>= 9), dpkg-dev (>= 1.17), xz-utils, dkms, libwayland-client0, libwayland-server0, libxext6 [!ppc64el], quilt, po-debconf, execstack, dh-modaliases, xserver-xorg-dev (>= 2:1.4), libglvnd-dev
Package-List:
 libnvidia-cfg1-410 deb non-free/libs optional arch=amd64
 libnvidia-common-410 deb non-free/libs optional arch=all
 libnvidia-compute-410 deb non-free/libs optional arch=i386,amd64
 libnvidia-decode-410 deb non-free/libs optional arch=i386,amd64
 libnvidia-encode-410 deb non-free/libs optional arch=i386,amd64
 libnvidia-fbc1-410 deb non-free/libs optional arch=i386,amd64
 libnvidia-gl-410 deb non-free/libs optional arch=i386,amd64
 libnvidia-ifr1-410 deb non-free/libs optional arch=i386,amd64
 nvidia-compute-utils-410 deb non-free/libs optional arch=amd64
 nvidia-dkms-410 deb non-free/libs optional arch=amd64
 nvidia-driver-410 deb non-free/libs optional arch=amd64
 nvidia-headless-410 deb non-free/libs optional arch=amd64
 nvidia-headless-no-dkms-410 deb non-free/libs optional arch=amd64
 nvidia-kernel-common-410 deb non-free/libs optional arch=amd64
 nvidia-kernel-source-410 deb non-free/libs optional arch=amd64
 nvidia-utils-410 deb non-free/libs optional arch=amd64
 xserver-xorg-video-nvidia-410 deb non-free/x11 optional arch=amd64
Checksums-Sha1:
 9c48c7dc2867c3836163078b5032efb102bc68c9 107912315 nvidia-graphics-drivers-410_410.78.orig-amd64.tar.gz
 ef86d2a487e134b557bdc4bec99b44b75ddc9beb 135 nvidia-graphics-drivers-410_410.78.orig.tar.gz
 f4ea43e6da182a89260c069ef4e6df48119b2f4f 34660 nvidia-graphics-drivers-410_410.78-0ubuntu1~gpu18.04.1.debian.tar.xz
Checksums-Sha256:
 0447ab26214b026730b7b9ce3be49bef7d61807b6580ae3ddb341bc7258d1ca1 107912315 nvidia-graphics-drivers-410_410.78.orig-amd64.tar.gz
 fcf9cd6af81947ba066be18c454f878c8e086286356f3a6e4444ef353e2224bb 135 nvidia-graphics-drivers-410_410.78.orig.tar.gz
 4ee92aebda7e20fa06cf331ab4c8fc12dafa148151f665748a4737c68592742f 34660 nvidia-graphics-drivers-410_410.78-0ubuntu1~gpu18.04.1.debian.tar.xz
Files:
 06589d89da4f2a3209c4696e9f831c6c 107912315 nvidia-graphics-drivers-410_410.78.orig-amd64.tar.gz
 0b041eae7d478fe964766f796acce51b 135 nvidia-graphics-drivers-410_410.78.orig.tar.gz
 42d208510867526f3287e876e4e3fd83 34660 nvidia-graphics-drivers-410_410.78-0ubuntu1~gpu18.04.1.debian.tar.xz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----

iQJOBAEBCgA4FiEE2Gs7Wj3LBZnyAANFA1NSfduWERoFAlv1oq8aHG1pY2hhZWxA
bWljaGFlbG1hcmxleS5jb20ACgkQA1NSfduWERo/Rg/9EpMczdqPZ6/uQefVkaNS
WbWaj/S4FYHWb+Yj9JLE8miI7YhnH25e0NbQNGQB/+jArujTwZLvIjydrIublaUw
QD6iGgWarE+dKEgF1v67mKXQW/rNP2ndQMmWAcyFpXYgYmYVxFFJcRWnxVjcl5cp
tIFzw7NDfp/KFXNBPPuz5jRQTgVyPt1rL+wkBljmDDx8yqNJFZKj4hxY4xBgg2PK
OoaGAit2iFIAysigbn/UAxsIEb1lL0CXBOGt/c7gw1yHPoeChOABxCj7noVkSHgu
B9wOZwoBdC26FfupVi7SahLdoYPepHhsr0XQe1dQPBc4LOzOGmi24+rBCGinMh5a
oJR0EWOWVplP3N/LgTZofL3EkJvb/tFYySTG1Hhji35IEnv0WD6xs2xyj2VeDkVI
NQyhJUhR253kkTE947+iZ/PUsmo7ADrVHUSyVcj26nCRt7fvLfYqdBDHivF8czYF
m3vV3jLwpmY5Xcp5fqdCh/KnX/MIH7HzpVavDIrhw7YdNiZLnBkP+adaNd2ZIwac
9Xz6OPI2bFT4eBBuvAg/HyoWWTU6gc7D3sFpytyO72XPD5VQXRffFxJ70yT3SQAN
A+cfkPwwysUoL1jjj1Dgud/63LUTjESScXBR9vKn2NCUkg1/DX3g5UacewevRm2y
8oJbd/X1an2hgdgTNulj2R0=
=3JuD
-----END PGP SIGNATURE-----
